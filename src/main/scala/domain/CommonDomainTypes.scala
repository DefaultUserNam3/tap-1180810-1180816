package domain

import scala.annotation.targetName

object CommonDomainTypes :

  val DEFAULT_PREFERENCE = 0;

  opaque type NonEmptyId = String
  object NonEmptyId:
    def from(id: String): Result[NonEmptyId] = if(id.nonEmpty) Right(id) else Left(DomainError.EmptyId(id))
    extension (id: NonEmptyId)
      @targetName("NonEmptyIdTo")
      def to: String = id

  opaque type NonNegativeInt = Int
  object NonNegativeInt:
    def from(i: Int): Result[NonNegativeInt] = if(i>=0) Right(i) else Left(DomainError.NegativeInt(i))
  extension (p: NonNegativeInt)
    @targetName("NonNegativeIntTo")
    def to: Int = p

  opaque type PositiveInt = Int
  object PositiveInt:
    def from(i: Int): Result[PositiveInt] = if(i>0) Right(i) else Left(DomainError.NonPositiveInt(i))
  extension (p: PositiveInt)
    @targetName("PositiveIntTo")
    def to: Int = p

  opaque type Preference = Int
  object Preference:
    val MIN_PREFERENCE= -2
    val MAX_PREFERENCE = 2
  
    def from(s: Int): Result[Preference] = if(s >= MIN_PREFERENCE && s<= MAX_PREFERENCE) Right(s) else Left(DomainError.PreferenceValueOutOfRange(s))
    extension (s: Preference)
      @targetName("PreferenceTo")
      def to: Int = s

  opaque type Day = Int
  object Day:
    val MONDAY = 1;
  
    def from(d: Int): Result[Day] = if(d >= MONDAY && d <= MONDAY + 6) Right(d) else Left(DomainError.InvalidDay(d))
    extension (d: Day)
      @targetName("DayTo")
      def to: Int = d

  object Week:
    def getWeek(): Seq[Day] = Seq(Week.monday.value,Week.tuesday.value,Week.wednesday.value,
      Week.thursday.value,Week.friday.value,Week.saturday.value,Week.sunday.value)

  enum Week(val value: Day):
    case monday extends Week(1)
    case tuesday extends Week(2)
    case wednesday extends Week(3)
    case thursday extends Week(4)
    case friday extends Week(5)
    case saturday extends Week(6)
    case sunday extends Week(7)
