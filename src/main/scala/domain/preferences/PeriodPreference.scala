package domain.preferences

import scala.annotation.targetName
import domain.Result
import domain.nurse.Role
import domain.schedulingPeriod.SchedulingPeriod
import domain.DomainError
import xml.XML.fromAttribute
import domain.DomainError.*
import domain.nurse.Nurse
import domain.CommonDomainTypes.Preference
import domain.Utils.{findNurse, findSchedulingPeriod}

import scala.xml.Node
import scala.xml.XML
import scala.xml.Elem
import xml.XML.traverse

final case class PeriodPreference (nurse: Nurse, period: SchedulingPeriod, preference: Preference)
object PeriodPreference:

  val nurse: String = "nurse"
  val schedulingPeriod: String = "period"
  val prefValue: String = "value"

  def from(nurse: Nurse, period: SchedulingPeriod, preference: Int): Result[PeriodPreference] =
    for {
      p <- Preference.from(preference)
    } yield PeriodPreference(nurse,period,p)


  def from (lsp: List[SchedulingPeriod])(ln:List[Nurse])(xml: Node): Result[PeriodPreference] =
    for
      n <- fromAttribute(xml, nurse)
      nurse1 <- findNurse(ln)(n)
      sp <- fromAttribute(xml, schedulingPeriod)
      sp1 <- findSchedulingPeriod(lsp)(sp)
      pref <- fromAttribute(xml, prefValue)
      prefVal <- Preference.from(pref.toInt)
    yield PeriodPreference(nurse1,sp1,prefVal)

