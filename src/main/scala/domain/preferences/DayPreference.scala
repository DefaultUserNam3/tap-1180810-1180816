package domain.preferences

import scala.annotation.targetName
import domain.Result
import domain.nurse.Role
import domain.schedulingPeriod.SchedulingPeriod
import domain.DomainError
import xml.XML.fromAttribute
import domain.DomainError.*
import domain.nurse.Nurse
import domain.CommonDomainTypes.Preference
import domain.CommonDomainTypes.Day
import domain.Utils.{findNurse, findSchedulingPeriod}

import scala.xml.Node
import scala.xml.XML
import scala.xml.Elem
import xml.XML.traverse

final case class DayPreference (nurse: Nurse, day: Day, preference: Preference)
object DayPreference:

  val nurse: String = "nurse"
  val day: String = "day"
  val prefValue: String = "value"

  def from(nurse: Nurse, day: Int, preference: Int): Result[DayPreference] =
    for {
      d <- Day.from(day)
      p <- Preference.from(preference)
    } yield DayPreference(nurse,d,p)


  def from (ln:List[Nurse])(xml: Node): Result[DayPreference] =
    for
      n <- fromAttribute(xml, nurse)
      nurse1 <- findNurse(ln)(n)
      d <- fromAttribute(xml, day)
      dVal <- Day.from(d.toInt)
      pref <- fromAttribute(xml, prefValue)
      prefVal <- Preference.from(pref.toInt)
    yield DayPreference(nurse1,dVal,prefVal)


