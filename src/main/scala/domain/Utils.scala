package domain

import domain.DomainError.UnknownNurse
import domain.DomainError.UnknownSchedulingPeriod
import domain.nurse.Nurse
import domain.nurse.Role
import domain.Result
import domain.schedulingPeriod.SchedulingPeriod

object Utils:
  def findNurse(ln: List[Nurse])(nurse: String): Result[Nurse] =
      for
        nurse1 <- ln.find(nr => nr.name.to == nurse).fold(Left(UnknownNurse(nurse)))(nr => Right(nr))
      yield nurse1

  def findSchedulingPeriod(lsp: List[SchedulingPeriod])(id: String): Result[SchedulingPeriod] =
    for
      sp1 <- lsp.find(sp => sp.id.to == id).fold(Left(UnknownSchedulingPeriod(id)))(sp => Right(sp))
    yield sp1
