package domain.schedule

import domain.CommonDomainTypes.{Day, Week}

import scala.xml.Elem
import domain.{DomainError, Result}
import domain.nurse.Nurse
import domain.scheduleInfo.ScheduleInfo
import domain.shift.Shift
import domain.nurse.Role
import domain.preferences.DayPreference
import domain.schedulingPeriod.SchedulingPeriod
import domain.shiftSchedule.ShiftSchedule

import scala.Console.println
import scala.annotation.tailrec
import scala.language.postfixOps

object ScheduleMS03 extends Schedule:

  def create(xml: Elem): Result[Elem] =
    for {
      si <-  ScheduleInfo.from(xml)
      s <- generateSchedule(si)
    } yield exportToXML(s)

  def generateSchedule(scheduleInfo: ScheduleInfo) : Result[ShiftSchedule]  =
    for{
      seq <- iterSchedulingPeriods(scheduleInfo.listSchedulingPeriods,List[List[Shift]](),scheduleInfo)
      res <- buildWeek(seq,scheduleInfo)
    } yield res

  private def iterSchedulingPeriods(schedulingPeriodsList : List[SchedulingPeriod], shifts: List[List[Shift]], scheduleInfo: ScheduleInfo) : Result[Seq[(Seq[Shift], Int)]]  =
    if(schedulingPeriodsList.isEmpty)
      val combs = combinationsShifts(shifts,scheduleInfo);
      Right(combs.map(ls => (ls.toSeq,ls.map(s => s.preferenceValue).sum)))  //.sortWith(_._2 > _._2))
    else
      val currentPeriod = schedulingPeriodsList.head
      iterNurseRequirements(currentPeriod,currentPeriod.nurseRequirements.to, List[List[List[(Nurse,Role)]]](), scheduleInfo).fold(
        err => Left(err), newShifts =>
          val nl = shifts ::: List(newShifts)
          iterSchedulingPeriods(schedulingPeriodsList.tail,nl,scheduleInfo)
      )

  private def iterNurseRequirements(currentPeriod : SchedulingPeriod,nurseRequirementsList : Map[Role, Int], schedulingPeriodCombinations: List[List[List[(Nurse,Role)]]], scheduleInfo: ScheduleInfo) : Result[List[Shift]]  =
    if(nurseRequirementsList.isEmpty)

      getListPossibleShifts(currentPeriod,schedulingPeriodCombinations,scheduleInfo).partitionMap(identity) match
        case (Nil, rights)       => Right(rights)
        case (firstLeft :: _, _) => Left(firstLeft)

    else
      val currentRequirement = nurseRequirementsList.head
      getAllCombinations(currentRequirement, scheduleInfo).fold(
        err => Left(err), listCombinationsReq => {
          val newlistCombinationsReqs = schedulingPeriodCombinations ::: List(listCombinationsReq)
          iterNurseRequirements(currentPeriod, nurseRequirementsList.tail, newlistCombinationsReqs,scheduleInfo)
        }
      )

  def getListPossibleShifts(currentPeriod : SchedulingPeriod,schedulingPeriodCombinations: List[List[List[(Nurse,Role)]]], scheduleInfo: ScheduleInfo) : List[Result[Shift]] =
    val combs = combinationList(schedulingPeriodCombinations)
    for{
      comb<- combs 
      //verify nurses repetidas
      if(comb.flatten.toMap.size == comb.flatten.size)
    } yield Shift.from(currentPeriod.id.to,comb.flatten.toMap,
      scheduleInfo.listPeriodPreferences.filter(p => p.period == currentPeriod && comb.flatten.map((n,_) => n).contains(p.nurse)).map(p => p.preference.to * p.nurse.seniority.to).sum).fold(
        a => Left(a), newShift => {
          Right(newShift)
        })

  def combinationsShiftsSup(a: List[List[Shift]],b:List[Shift],scheduleInfo: ScheduleInfo) = a match
    case List() => for(i <- b) yield List(i)
    case xs => for{ x <- xs; y<- b

                    temp = x :+ y
                    if({
                      val list = temp.map(s => s.nurses.to.toList.map( (n,_) => n._1)).flatten.groupBy(identity).map(n => (n._1,n._2.length))
                      list.forall(_._2 <= scheduleInfo.constraints.maxShiftsPerDay.to)
                    })

                    } yield x :+y

  def combinationsShifts(ls: List[List[Shift]],scheduleInfo: ScheduleInfo) =  ls.foldLeft(List(List[Shift]()))((l,x) => combinationsShiftsSup(l,x,scheduleInfo))

  def combinationsWeekSup(si: ScheduleInfo,n: Int, l: Seq[(Seq[Shift],Int,Set[Nurse])]): Seq[Seq[(Seq[Shift],Int,Set[Nurse])]] =
    n match
      case 0 => Seq(Seq())
      case _ => for {el <- l;
                     sl <- combinationsWeekSup(si,n - 1, l dropWhile {_ != el})

                     temp = el +: sl
                     if ({
                       if(7-temp.size <  si.constraints.minRestDaysPerWeek.to ) {
                         // Obtain a flattened and distinct list of all nurses that work the schedule
                         val nursesDistinct = temp.map((s, i, ln) => ln).flatten.distinct

                         // Count the days in which nurse does not work, check if their higher or equal to the minimum rest days
                         nursesDistinct.forall(n => temp.count((_, _, l) => !l.contains(n)) >= si.constraints.minRestDaysPerWeek.to)
                       }else{ true}
                     }
                       )
                     } yield el +: sl

  def combinationsWeek(si: ScheduleInfo,n: Int, l: Seq[(Seq[Shift],Int,Set[Nurse])]): Seq[Seq[(Seq[Shift],Int,Set[Nurse])]] = combinationsWeekSup(si,n, l.distinct)

  def combinationList[T](ls:List[List[T]]):List[List[T]] = ls match
    case Nil => Nil::Nil
    case head :: tail => val rec = combinationList[T](tail)
      rec.flatMap(r => head.map(t => t::r))


  private def getAllCombinations(requirement : (Role, Int), scheduleInfo: ScheduleInfo): Result[List[List[(Nurse,Role)]]] =

    val listViableNurses : List[Nurse] = scheduleInfo.listNurses.filter(n=> n.roles.to.contains(requirement._1))

    if(listViableNurses.isEmpty || listViableNurses.length < requirement._2) Left(DomainError.NotEnoughNurses)

    else
      val combinations = listViableNurses.combinations(requirement._2).map(ln => ln.map( n => (n,requirement._1))).toList
      Right(combinations)


  /**
   *
   * 1. Gera todas as combinacoes semanais validas primeiro
   * 2. Avalia em termos de pesos de preferencia de dia
   * 3. Avalia em termos de pesos de preferencia de scheduling period (caso haja mais que 1 com o melhor weight para desempatar)
   *
   * @param dayPossib
   * @param si
   * @return
   */
  def buildWeek(dayPossib : Seq[(Seq[Shift], Int)], si : ScheduleInfo) : Result[ShiftSchedule] =
    print("BuildWeekSize ")
    print(dayPossib.length)
    if(dayPossib.isEmpty) return Left(DomainError.NotEnoughNurses)

    val filteredDayPossib = filterOutSimilarDayByPeriodPref(dayPossib)
    print("\nFilteredWeekSize ")
    print(filteredDayPossib.length)

    val combs = combinationsWeek(si,7,filteredDayPossib)
    println("\nCombinationsWeek ")
    println(combs.length)
    if(combs.isEmpty) return Left(DomainError.NotEnoughNurses)

    val selectedWeeks = getBestPermutations(combs,si).sortBy(_._2).reverse

    val finalWeeks = selectedWeeks.filter(elem => elem._2 == selectedWeeks.head._2).sortWith(_._1.values.map(elem => elem._2).sum > _._1.values.map(elem => elem._2).sum)
    val filteredfinalWeek = finalWeeks.filter(elem => elem._1.values.map(elem => elem._2).sum == finalWeeks.head._1.values.map(elem => elem._2).sum).reverse.head
    ShiftSchedule.from(filteredfinalWeek._1.map(elem => (elem._1 -> elem._2._1)),  filteredfinalWeek._2)


  def getBestPermutations( weekCombinations : Seq[Seq[(Seq[Shift],Int,Set[Nurse])]], si: ScheduleInfo) : Seq[(Map[Day,(Seq[Shift],Int,Set[Nurse])], Int)] =
    weekCombinations.map(wc => getBestPermutationsSup(wc, si))

  def getBestPermutationsSup( weekCombination : Seq[(Seq[Shift],Int,Set[Nurse])], si : ScheduleInfo) : (Map[Day,(Seq[Shift],Int,Set[Nurse])], Int) =
    val perms = genWeekPermutations(weekCombination.zipWithIndex,Map(),Week.getWeek())
    val permsWeigth = perms.map((elem) => (elem,calcScheduleDayWeight(elem, si)))
    val max = permsWeigth.maxBy(_._2)._2
    val bestPerm = permsWeigth.filter(elem => elem._2 == max).head
    bestPerm

  def genWeekPermutations(weekDCLeft : Seq[((Seq[Shift],Int,Set[Nurse]),Int)], assignedWeek : Map[Day,(Seq[Shift],Int,Set[Nurse])], daysLeft: Seq[Day]) : Seq[Map[Day,(Seq[Shift],Int,Set[Nurse])]] =
    if(weekDCLeft.isEmpty) Seq(assignedWeek)
    else
      weekDCLeft.foldLeft[Seq[Map[Day,(Seq[Shift],Int,Set[Nurse])]]](Seq())((accum, elem) => { accum ++ genWeekPermutations(weekDCLeft.filter(w => w._2!=elem._2),assignedWeek + (daysLeft.head -> elem._1),daysLeft.tail) })

  /**
   * Filters out similar day combinations (day combinations with the same nurse sets) by evaluating their period preference
   * @param dayPossib
   * @return
   */
  def filterOutSimilarDayByPeriodPref(dayPossib : Seq[(Seq[Shift], Int)]) : Seq[(Seq[Shift], Int, Set[Nurse])] =
    val dayPossibWNurses = getCombinationNurseSets(dayPossib)
    dayPossibWNurses.zipWithIndex.filter( // N?o tem de validar o pr?prio elem. N?o tem de validar se s?o sets de nurses diferentes Se for o mesmo set de nurses, valida se o weight ? maior. Se for igual escolhe o elem com maior hash
      (el) => {
        val elem = el._1
        val indx = el._2
        dayPossibWNurses.zipWithIndex.forall(
          el2 => {
            val elem2 = el2._1
            val indx2 = el2._2
            elem == elem2 || elem._3 != elem2._3 || (elem._3 == elem2._3 && (elem._2>elem2._2 || (elem._2 == elem2._2 && indx < indx2)))} ) }).map(a => a._1)

  /**
   * Appends to a sequence of day combinations and it's period weight the set of nurses that work in that day combination
   * @param dayPossib
   * @return
   */
  def getCombinationNurseSets(dayPossib : Seq[(Seq[Shift], Int)]) : Seq[(Seq[Shift],Int,Set[Nurse])] =
    dayPossib.map(elem => (elem._1, elem._2, elem._1.map(s => s.nurses.to.keySet).flatten.toSet))

  def calcScheduleDayWeight(schedule: Map[Day,(Seq[Shift],Int,Set[Nurse])], si: ScheduleInfo) : Int =
    schedule.foldLeft(0)((accum,elem) => accum + calcComboDayWeight(elem._1,elem._2._3,si))

  def calcComboDayWeight(day : Day, nurses: Set[Nurse], si: ScheduleInfo): Int =
    nurses.foldLeft(0)(
      (accum, elem) => accum + {
        val l = si.listDayPreference.filter( dp => dp.day == day && dp.nurse == elem )
        if(l.isEmpty) 0
        else l.head.preference.to * elem.seniority.to
      }
    )

  private def exportToXML(shiftSchedule: ShiftSchedule): Elem =
    ShiftSchedule.exportToXML(shiftSchedule)




