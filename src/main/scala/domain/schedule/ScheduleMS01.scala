package domain.schedule

import domain.CommonDomainTypes.Day

import scala.xml.Elem
import domain.Result
import domain.DomainError
import domain.nurse.Nurse
import domain.nurse.Role
import domain.scheduleInfo.ScheduleInfo
import domain.schedulingPeriod.*
import domain.shift.Shift
import domain.shiftSchedule.ShiftSchedule

import scala.annotation.tailrec


object ScheduleMS01 extends Schedule:

  def create(xml: Elem): Result[Elem] =
    ScheduleInfo.from(xml) match
      case Left(a) => Left(a)
      case Right(b) => generateSchedule(b) match
        case Left(a) => Left(a)
        case Right(b) => Right(exportToXML(b))

  def generateSchedule(scheduleInfo: ScheduleInfo) : Result[ShiftSchedule] =
    val shifts = Map[Day,List[Shift]]()
    for {
      currentDay <- Day.from(1)
      limitDay <- Day.from(7)
      res <- generateScheduleSup(shifts,scheduleInfo,currentDay,limitDay)
    } yield res

  private def generateScheduleSup(shifts: Map[Day,List[Shift]], scheduleInfo: ScheduleInfo, currentDay: Day, limitDay: Day) : Result[ShiftSchedule] =
    generateScheduleDay(shifts,scheduleInfo,currentDay).fold(
      a => Left(a),
      b => if(currentDay.to >= limitDay.to) ShiftSchedule.from(b,0)
      else for{
        day <- Day.from(currentDay.to+1)
        res <-  generateScheduleSup(b, scheduleInfo, day, limitDay)
      } yield res
    )


  /**
   * Generates the schedule for a given day
   *
   * @param shifts map with the previously added day shifts
   * @param scheduleInfo Object that holds all the input info
   * @param day target day for the schedule
   * @return
   */
  private def generateScheduleDay(shifts: Map[Day,List[Shift]], scheduleInfo: ScheduleInfo, day: Day) : Result[Map[Day,List[Shift]]] =
    // For each scheduling period
    iterSchedulingPeriods(scheduleInfo.listSchedulingPeriods,shifts,scheduleInfo,day)

  /**
   * Iterates recursively over the scheduling periods recursively, calculating the shifts in the process
   * @param schedulingPeriodsList
   * @param shifts
   * @param scheduleInfo
   * @param day
   * @return
   */

  private def iterSchedulingPeriods(schedulingPeriodsList : List[SchedulingPeriod], shifts: Map[Day,List[Shift]], scheduleInfo: ScheduleInfo, day: Day) : Result[Map[Day,List[Shift]]] =
    if(schedulingPeriodsList.isEmpty) Right(shifts)
    else
      val currentPeriod = schedulingPeriodsList.head
      iterNurseRequirements(currentPeriod,currentPeriod.nurseRequirements.to, Map[Nurse,Role](), shifts, scheduleInfo, day).fold(
        err => Left(err), newShifts => iterSchedulingPeriods(schedulingPeriodsList.tail,newShifts,scheduleInfo,day)
      )

  /**
   * Iterates recursively over the list of nurse requirements of a scheduling period, calculating the shift of this scheduling period in the process
   * @param nurseRequirementsList
   * @param shifts
   * @param scheduleInfo
   * @param day
   * @return
   */

  private def iterNurseRequirements(currentPeriod : SchedulingPeriod,nurseRequirementsList : Map[Role, Int], currentPeriodNurses: Map[Nurse,Role], shifts: Map[Day,List[Shift]], scheduleInfo: ScheduleInfo, day: Day) : Result[Map[Day,List[Shift]]] =
    if(nurseRequirementsList.isEmpty)
      val removedDayMap = shifts.-(day)
      val oldShiftsList = shifts.getOrElse(day, List[Shift]())

      Shift.from(currentPeriod.id.to,currentPeriodNurses,0).fold(
        a => Left(a), newShift => {
          val newDayShifts = oldShiftsList ::: List(newShift)
          Right(removedDayMap.+((day,newDayShifts)))
        }
      )
    else
      val currentRequirement = nurseRequirementsList.head
      findViableNurses(currentRequirement, currentPeriodNurses, shifts, scheduleInfo, day).fold(
        err => Left(err), newNurseList => {
          iterNurseRequirements(currentPeriod, nurseRequirementsList.tail, newNurseList,shifts,scheduleInfo,day)
        }
      )

  /**
   * Iterates recursively over the list of nurses finding the first that fits the requirements (does so until the amount of required nurses is obtained)
   * @param requirement
   * @param addedNurses
   * @param shifts
   * @param scheduleInfo
   * @param day
   * @return
   */
  @tailrec
  private def findViableNurses(requirement : (Role, Int),addedNurses: Map[Nurse, Role],shifts: Map[Day,List[Shift]], scheduleInfo: ScheduleInfo, day: Day) : Result[Map[Nurse, Role]] =
    if(requirement._2 == 0) return Right(addedNurses)

    val listViableNurses : List[Nurse] = scheduleInfo.listNurses.filter(n=> n.roles.to.contains(requirement._1) && !addedNurses.contains(n) && checkShiftsNurse(n,shifts,day, scheduleInfo) )
    if(listViableNurses.isEmpty) Left(DomainError.NotEnoughNurses)
    else findViableNurses((requirement._1,requirement._2-1),addedNurses + (listViableNurses.head-> requirement._1),shifts, scheduleInfo, day)


  private def checkShiftsNurse(nurse : Nurse, shifts: Map[Day,List[Shift]], day: Day, scheduleInfo: ScheduleInfo) : Boolean =
    shifts.getOrElse(day,List[Shift]()).count(shift => shift.nurses.to.contains(nurse)) < scheduleInfo.constraints.maxShiftsPerDay.to




  private def exportToXML(shiftSchedule: ShiftSchedule): Elem =
      ShiftSchedule.exportToXML(shiftSchedule)
   

