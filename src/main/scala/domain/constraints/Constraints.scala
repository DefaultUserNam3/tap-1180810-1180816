package domain.constraints

import domain.Result
import domain.CommonDomainTypes.{PositiveInt, *}
import xml.XML.fromAttribute

import scala.xml.Node
import scala.xml.XML
import scala.xml.Elem

final case class Constraints (minRestDaysPerWeek: NonNegativeInt, maxShiftsPerDay: PositiveInt)
object Constraints:
  val minRestDaysPerWeekXML: String = "minRestDaysPerWeek"
  val maxShiftsPerDayXML: String = "maxShiftsPerDay"

  def from(minRestDaysPerWeek : Int, maxShiftsPerDay: Int) : Result[Constraints] =
    for
      mrd <- NonNegativeInt.from(minRestDaysPerWeek)
      msd <- PositiveInt.from(maxShiftsPerDay)
    yield Constraints(mrd, msd)

  def from (xml: Node): Result[Constraints] =
    for
      mrdw <- fromAttribute(xml, minRestDaysPerWeekXML)
      cMrdw <- NonNegativeInt.from(mrdw.toInt)
      mspd <- fromAttribute(xml, maxShiftsPerDayXML)
      cMspd <- PositiveInt.from(mspd.toInt)
    yield Constraints(cMrdw,cMspd)

