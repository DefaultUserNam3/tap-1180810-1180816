package domain.schedulingPeriod

import domain.CommonDomainTypes.NonEmptyId
import domain.DomainError
import domain.Result
import domain.nurse.Role

import scala.annotation.targetName
import xml.XML.fromAttribute
import domain.DomainError.*

import scala.xml.Node
import scala.xml.XML
import scala.xml.Elem
import xml.XML.traverse

final case class SchedulingPeriod (id : NonEmptyId, start : ValidTime, end : ValidTime, nurseRequirements : NonEmptyRoleMap)
object SchedulingPeriod:

  val spId: String = "id"
  val start: String = "start"
  val end: String = "end"
  val nurseReqs: String = "nurseRequirement"

  def from(id : String, start: String, end: String, nurseRequirements: Map[Role,Int]) : Result[SchedulingPeriod] =
    for
      i <- NonEmptyId.from(id)
      s <- ValidTime.from(start)
      e <- ValidTime.from(end)
      nr <- NonEmptyRoleMap.from(nurseRequirements)
    yield SchedulingPeriod(i, s, e, nr)

  def from (lnr: List[Role])(xml: Node): Result[SchedulingPeriod] =
    for
      spid <- fromAttribute(xml, spId)
      id <- NonEmptyId.from(spid)
      d1 <- fromAttribute(xml, start)
      start2 <- ValidTime.from(d1)
      d2 <- fromAttribute(xml, end)
      end2 <- ValidTime.from(d2)
      nurseReqsRoles <- traverse( (xml \\ nurseReqs), (n: Node) => fromAttribute(n,"role") )
      nurseReqsNumbers <- traverse( (xml \\ nurseReqs), (n: Node) =>  fromAttribute(n,"number") )
      listRoles <- validateNurseReqsRoles(lnr)(nurseReqsRoles)
      map <- NonEmptyRoleMap.from((listRoles zip nurseReqsNumbers.map((s: String) => s.toInt)).toMap)
    yield SchedulingPeriod(id,start2,end2,map)

def validateNurseReqsRoles(lnr: List[Role])(nurseReqsRoles: List[String]): Result[List[Role]] =
  nurseReqsRoles.map(role =>
    for
      role <- lnr.find(pr => pr.to == role).fold(Left(UnknownRequirementRole(role)))(pr => Right(pr))
    yield role
  ).partitionMap(identity) match
    case (Nil, rights)       => Right(rights)
    case (firstLeft :: _, _) => Left(firstLeft)



opaque type ValidTime = String
object ValidTime:
  val acceptedTimeFormat = "^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$"; // Corresponde a HH:mm:ss
  def from(time: String): Result[ValidTime] =
    if(time.matches(acceptedTimeFormat))
      Right(time)
    else Left(DomainError.InvalidTime(time))
  extension (time: ValidTime)
    @targetName("ValidTimeTo")
    def to: String = time

opaque type NonEmptyRoleMap = Map[Role,Int]
object NonEmptyRoleMap:
  def from(map: Map[Role,Int]): Result[NonEmptyRoleMap] =
    if(map.size > 0) Right(map) else Left(DomainError.EmptyRoleMap(map))
  extension (map: NonEmptyRoleMap)
    @targetName("NonEmptyRoleMapTo")
    def to: Map[Role,Int] = map