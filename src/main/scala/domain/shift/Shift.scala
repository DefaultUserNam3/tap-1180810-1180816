package domain.shift

import domain.CommonDomainTypes.NonEmptyId
import domain.DomainError
import domain.nurse.Nurse
import domain.nurse.Role
import domain.Result

import scala.annotation.targetName
import scala.xml.Elem


final case class Shift private (id : NonEmptyId, nurses : NonEmptyNurseRoleMap, preferenceValue : Int)
object Shift:
  def from(id : String,  nurses:  Map[Nurse,Role], preferenceValue : Int) : Result[Shift] =
    for
      i <- NonEmptyId.from(id)
      nl <- NonEmptyNurseRoleMap.from(nurses)
    yield Shift(i, nl,preferenceValue)

  def exportToXML(shift: Shift): Elem =
    <shift id={shift.id.to}>
          {shift.nurses.to.toSeq.sortBy(_._1.name.to).map(Elem => Nurse.exportToXML(Elem))}
    </shift>


opaque type NonEmptyNurseRoleMap = Map[Nurse,Role]
object NonEmptyNurseRoleMap:
  def from(map: Map[Nurse,Role]): Result[NonEmptyNurseRoleMap] =
    if(map.size > 0) Right(map) else Left(DomainError.EmptyNurseRoleMap(map))
  extension (map: NonEmptyNurseRoleMap)
    @targetName("NonEmptyNurseRoleMapTo")
    def to: Map[Nurse,Role] = map