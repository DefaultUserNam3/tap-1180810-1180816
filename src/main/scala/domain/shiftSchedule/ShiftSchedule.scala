package domain.shiftSchedule

import domain.DomainError
import domain.nurse.Nurse
import domain.CommonDomainTypes.Day
import domain.Result
import domain.shift.Shift
import scala.xml.Elem


import scala.annotation.targetName

final case class ShiftSchedule private (mapShifts : NonEmptyShiftsMap, preferenceValue : Int)
object ShiftSchedule:
  def from(mapShifts : Map[Day,Seq[Shift]],  preferenceValue : Int) : Result[ShiftSchedule] =
    for
      nl <- NonEmptyShiftsMap.from(mapShifts)
    yield ShiftSchedule(nl, preferenceValue)

  def exportToXML(shiftSchedule: ShiftSchedule): Elem =
    <shiftSchedule xmlns="http://www.dei.isep.ipp.pt/tap-2022" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dei.isep.ipp.pt/tap-2022 ../../schedule.xsd ">
      {shiftSchedule.mapShifts.to.toSeq.sortBy(_._1.to).map(Elem => exportToXMLHelper(Elem))}
    </shiftSchedule>

  def exportToXMLHelper(daySchedule:(Day,Seq[Shift])): Elem =
    <day id={daySchedule._1.toString}>
      {daySchedule._2.map(Elem => Shift.exportToXML(Elem))}
    </day>


opaque type NonEmptyShiftsMap = Map[Day,Seq[Shift]]
object NonEmptyShiftsMap:
  def from(map: Map[Day,Seq[Shift]]): Result[NonEmptyShiftsMap] =
    if(map.size > 0) Right(map) else Left(DomainError.EmptyShiftsMap(map))
  extension (map: NonEmptyShiftsMap)
    @targetName("NonEmptyShiftsMapTo")
    def to: Map[Day,Seq[Shift]] = map