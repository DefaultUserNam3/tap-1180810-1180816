package domain.scheduleInfo

import domain.nurse.Nurse
import domain.nurse.Role
import domain.constraints.Constraints
import domain.schedulingPeriod.SchedulingPeriod

import scala.xml.Node
import domain.Result
import domain.preferences.{DayPreference, PeriodPreference}
import xml.XML.traverse

final case class ScheduleInfo(listSchedulingPeriods: List[SchedulingPeriod],
                              listNurses: List[Nurse], listPeriodPreferences: List[PeriodPreference],
                              listDayPreference: List[DayPreference], constraints: Constraints
                             )

/**
* Object that will aggregate all info extracted from the xml file
**/
object ScheduleInfo:

  val nurseNode: String = "nurse"
  val nurseRoleNode: String = "nurseRole"
  val schedulingPeriodNode: String = "schedulingPeriod"
  val constraintsNode: String = "constraints"

  val periodPreferenceNode: String = "periodPreference"
  val dayPreferenceNode: String = "dayPreference"


  /**
   *
   * @param xml
   * @return
   */
  def from(xml: Node): Result[ScheduleInfo] =
    for {
      lnr <- traverse( (xml \\ nurseRoleNode), (n: Node) => Role.from(n))
      lsp <- traverse( (xml \\ schedulingPeriodNode), (n: Node) => SchedulingPeriod.from(lnr.distinct)(n))
      ln <- traverse( (xml \\ nurseNode), (n: Node) => Nurse.from(lnr.distinct)(n))
      lpp <- traverse( (xml \\ periodPreferenceNode), (n: Node) => PeriodPreference.from(lsp)(ln)(n))
      ldp <- traverse( (xml \\ dayPreferenceNode), (n: Node) => DayPreference.from(ln)(n))
      lc <- traverse( (xml \\ constraintsNode), (n: Node) => Constraints.from(n))


    } yield ScheduleInfo(lsp,ln,lpp,ldp,lc.head)

