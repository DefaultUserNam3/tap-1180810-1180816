package domain.nurse

import scala.annotation.targetName
import domain.Result
import domain.schedulingPeriod.SchedulingPeriod
import domain.DomainError
import xml.XML.fromAttribute
import domain.DomainError.*
import domain.nurse.Nurse.nurseRoleAttr

import scala.xml.Node
import scala.xml.XML
import scala.xml.Elem
import xml.XML.traverse

final case class Nurse (name : NonEmptyName, seniority : Seniority, roles : NonEmptyRoleList)
object Nurse:

  val name: String = "name"
  val seniority: String = "seniority"
  val nurseRole: String = "nurseRole"
  val nurseRoleAttr: String = "role"

  def from(name : String, seniority: Int, roles: List[Role]) : Result[Nurse] =
    for
      n <- NonEmptyName.from(name)
      s <- Seniority.from(seniority)
      rs <- NonEmptyRoleList.from(roles)
    yield Nurse(n, s, rs)

  def from (lnr: List[Role])(xml: Node): Result[Nurse] =
    for
      nurseName <- fromAttribute(xml, name).fold[Result[NonEmptyName]](_ => Left(DomainError.EmptyName), n1 => NonEmptyName.from(n1))
      s <- fromAttribute(xml, seniority)
      sen <- Seniority.from(s.toInt)
      nurseRoles <- traverse( (xml \\ nurseRole), (n: Node) => Role.from(fromAttribute(n,nurseRoleAttr).getOrElse("")) )
      nurseRolesValidated <- NonEmptyRoleList.from(nurseRoles)
    yield Nurse(nurseName,sen,nurseRolesValidated)

  def exportToXML(nurse: (Nurse,Role)): Elem =
    <nurse name={nurse._1.name.to} role={nurse._2.to} />



opaque type Seniority = Int
object Seniority:
  val MIN_SENIORITY = 1
  val MAX_SENIORITY = 5

  def from(s: Int): Result[Seniority] = if(s >= MIN_SENIORITY && s<= MAX_SENIORITY) Right(s) else Left(DomainError.SeniorityValueOutOfRange(s))
  extension (s: Seniority)
    @targetName("SeniorityTo")
    def to: Int = s

opaque type NonEmptyName = String
object NonEmptyName:
  def from(name: String): Result[NonEmptyName] = if(name.trim.nonEmpty) Right(name) else Left(DomainError.EmptyName)
  extension (name: NonEmptyName)
    @targetName("NonEmptyNameTo")
    def to: String = name

opaque type Role = String
object Role:
  def from(xml: Node): Result[Role] =
    for
      rt <- fromAttribute(xml, nurseRoleAttr)
      t <- NonEmptyName.from(rt)
    yield t
  def from(title : String): Result[Role] = if(title.nonEmpty) Right(title) else Left(DomainError.EmptyTitle(title))
  extension (role: Role)
    @targetName("RoleTo")
    def to: String = role

opaque type NonEmptyRoleList = List[Role]
object NonEmptyRoleList:

  def from(roles : List[Role]): Result[NonEmptyRoleList] = if(roles.nonEmpty) Right(roles) else Left(DomainError.EmptyRoleList(roles))
  extension (roles: NonEmptyRoleList)
    @targetName("NonEmptyRoleListTo")
    def to: List[Role] = roles

