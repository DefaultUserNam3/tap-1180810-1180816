//package domain.role
//
//import domain.DomainError
//import domain.Result
//import scala.annotation.targetName
//import xml.XML.fromAttribute
//import domain.DomainError.*
//import scala.xml.Node
//import scala.xml.XML
//import scala.xml.Elem

//final case class Role (title : NonEmptyTitle)
//object Role:
//
//  val roleTitle: String = "role"
//
//  def from(title : String) : Result[Role] =
//    for
//      t <- NonEmptyTitle.from(title)
//    yield Role(t)
//
//  def from(xml: Node): Result[Role] =
//    for
//      rt <- fromAttribute(xml, roleTitle)
//      t <- NonEmptyTitle.from(rt)
//    yield Role(t)
//
//
//
//
//opaque type NonEmptyTitle = String
//object NonEmptyTitle:
//  def from(title: String): Result[NonEmptyTitle] = if(title.nonEmpty) Right(title) else Left(DomainError.EmptyTitle(title))
//  extension (title: NonEmptyTitle)
//    @targetName("NonEmptyTitleTo")
//    def to: String = title




