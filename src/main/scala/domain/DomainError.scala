package domain

import domain.CommonDomainTypes.Day
import domain.nurse.Role
import domain.nurse.Nurse
import domain.shift.Shift
import domain.CommonDomainTypes.Day

type Result[A] = Either[DomainError,A]

enum DomainError:
  case IOFileProblem(error: String)
  case XMLError(error: String)
  case EmptyName
  case SeniorityValueOutOfRange(error: Int)
  case EmptyTitle(error: String)
  case EmptyRoleList(error: List[Role])
  case EmptyId(error: String)
  case InvalidTime(error: String)
  case EmptyRoleMap(error: Map[Role,Int])
  case EmptyNurseRoleMap(error: Map[Nurse,Role])
  case EmptyShiftsMap(error: Map[Day,Seq[Shift]])
  case UnknownRequirementRole(error: String)
  case UnknownNurse(error: String)
  case UnknownSchedulingPeriod(error: String)
  case NegativeInt(error: Int)
  case NonPositiveInt(error: Int)
  case NotEnoughNurses
  case PreferenceValueOutOfRange(error: Int)
  case InvalidDay(error: Int)