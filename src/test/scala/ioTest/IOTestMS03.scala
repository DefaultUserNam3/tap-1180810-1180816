package ioTest

import assessment.AssessmentBehaviours

class IOTestMS03 extends AssessmentBehaviours:
  val PATH = "files/filesStudents/AssessmentSubstitute"
//  val PATH = "files/filesStudents/ms03"


  performTests(assessment.AssessmentMS03.create, "Milestone 3")

