package ioTest

import assessment.AssessmentBehaviours

class IOTestMS01 extends AssessmentBehaviours:
  val PATH = "files/filesStudents/ms01"

  performTests(assessment.AssessmentMS01.create, "Milestone 1")

