package domain.schedulingPeriod

import domain.preferences.DayPreference
import domain.nurse.NurseProperties.*
import domain.nurse.{Nurse, Role}
import domain.CommonDomainTypesProperties.genNonEmptyId
import domain.DomainError
import domain.constraints.Constraints
import domain.constraints.ConstraintsProperties.genConstraints
import domain.preferences.DayPreferenceProperties.genLDayPreference
import domain.preferences.PeriodPreferenceProperties.genLPeriodPreference
import domain.Result
import domain.role.RoleProperties.genRole
import domain.schedule.ScheduleMS01
import domain.scheduleInfo.ScheduleInfo
import domain.schedulingPeriod.SchedulingPeriodProperties.{MAX_QUANTITY, MAX_ROLES}
import org.scalacheck.*

import scala.math.*
import scala.util.Random
import org.scalacheck.Test.Parameters
import org.scalacheck.Prop.forAll
import org.scalacheck.Properties

import scala.Console
import scala.collection.immutable.List.from
import scala.language.adhocExtensions

object SchedulingPeriodProperties extends Properties("SchedulingPeriodProperties") :
  
  val MAX_QUANTITY = 2
  val MAX_ROLES = 3
  val validTimes = List("08:00:00","16:00:00","00:00:00","23:30:00")

  def genSchedulingPeriod: Gen[SchedulingPeriod] =
    for {
      randomNonEmptyId <- genNonEmptyId
      startTime <- genValidTime
      endTime <- genValidTime
      nonEmptyRoleMap <- genNonEmptyRoleMap
    } yield SchedulingPeriod(randomNonEmptyId,startTime,endTime,nonEmptyRoleMap)

  def genSchedulingPeriod1Role : Gen[SchedulingPeriod] =
    for {
      randomNonEmptyId <- genNonEmptyId
      startTime <- genValidTime
      endTime <- genValidTime
      nonEmptyRoleMap <- genNonEmptyRoleMapLength1
    } yield SchedulingPeriod(randomNonEmptyId,startTime,endTime,nonEmptyRoleMap)

  def genLSchedulingPeriods: Gen[List[SchedulingPeriod]] =
    Gen.choose(1,2).flatMap(s => Gen.listOfN(s, genSchedulingPeriod))

  def genValidTime: Gen[ValidTime] =
    for {
      randomTime <- Gen.oneOf(validTimes)
      randomValidTime <- ValidTime.from(randomTime).fold(a => Gen.fail, b => Gen.const(b))
    } yield randomValidTime

  def genRoleNumMap: Gen[(Role,Int)] =
    for {
      randomNum <- Gen.chooseNum(1,MAX_QUANTITY)
      randomRole <- genRole
    } yield (randomRole -> randomNum)

  def genNonEmptyRoleMap: Gen[NonEmptyRoleMap] =
    for {
      limsRoleList <- Gen.chooseNum(1,MAX_ROLES)
      randomRoleMap <- Gen.mapOfN(limsRoleList,(genRoleNumMap))
      randomNonEmptyRoleMap <- NonEmptyRoleMap.from(randomRoleMap).fold(a => Gen.fail, b => Gen.const(b))
    } yield randomNonEmptyRoleMap

  def genNonEmptyRoleMapLength1: Gen[NonEmptyRoleMap] =
    for {
      randomRoleMapL1 <- Gen.mapOfN(1,genRoleNumMap)
      randomNonEmptyRoleMapL1 <- NonEmptyRoleMap.from(randomRoleMapL1).fold(a => Gen.fail, b => Gen.const(b))
    } yield randomNonEmptyRoleMapL1















