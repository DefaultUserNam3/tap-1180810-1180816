package domain.schedule

import domain.CommonDomainTypes.*
import domain.nurse.*
import domain.preferences.DayPreference
import domain.schedule.ScheduleProperties.property
import domain.scheduleInfo.ScheduleInfoProperties.*
import domain.shift.Shift
import org.scalacheck.{Properties, Test}
import org.scalacheck.Prop.forAll

object ScheduleProperties extends Properties("ScheduleProperties") :

  // schedule algorithm to test
  val scheduleAlg = ScheduleMS01

  override def overrideParameters(prms: Test.Parameters): Test.Parameters =
    prms.withMinSuccessfulTests(200)

  property("TEMP") =
    forAll(genValidScheduleInfo)( si => {
      val temp = ScheduleMS03.generateSchedule(si)
      print('\n')
      true
    })

 

/*

  property("A nurse cannot be used in the same shift with different roles") =
    forAll(genValidScheduleInfo)( si => {
      scheduleAlg.generateSchedule(si).fold(e => true, ss =>
        {
          // For all the shifts, check if the list of DISTINCT nurses has the same size as the list of nurses
          ss.mapShifts.to.toList.filter(t => !t._2.filter(s => s.nurses.to.toList.distinct.length != s.nurses.to.toList.length).isEmpty).isEmpty
        }
      )
    })


  property("The shift schedule must schedule all the requirements for each day.") =
    forAll(genValidScheduleInfo)( si => {
      scheduleAlg.generateSchedule(si).fold(e => true, ss =>
        {
          val lShifts : Map[Day,Seq[Shift]] = ss.mapShifts.to
          val lSPs = si.listSchedulingPeriods
          val weekDays = List.range(1,8)

          lSPs.forall(  // For each scheduling period
            sp => {
              sp.nurseRequirements.to.forall(
                nR => {
                  weekDays.forall(  // For each weekDay
                    wD => { // Check if in this day's shift list a shift corresponding to this scheduling period exists and if this role requirement is accomplished with the nurses of the shift
                      lShifts.exists((d,ls) => d.to == wD && ls.exists(s => s.id == sp.id && s.nurses.to.count((_, r) => r == nR._1) == nR._2))
                    }
                  )
                }
              )
            }
          )
        }
      )
    })

  property("A generated schedule must always have the days 1 to 7, no more, no less") =
    forAll(genValidScheduleInfo)( si => {
      scheduleAlg.generateSchedule(si).fold(e => true, ss => {
        val weekDays = List.range(1, 8)

        // Check if the map contains every weekday
        ss.mapShifts.to.forall(
          (d,_) => {
            weekDays.contains(d.to)
          }
        ) && ss.mapShifts.to.size == 7 // And also make sure there are no more than 7 days in the schedule
      })
    })

  property("The nurses should not work more shifts than the quantity defined by maxShiftsPerDay") =
    forAll(genValidScheduleInfo)( si => {
      scheduleAlg.generateSchedule(si).fold(e => true, ss => {
        val lShifts: Map[Day, Seq[Shift]] = ss.mapShifts.to
        val mspd = si.constraints.maxShiftsPerDay
        val weekDays = List.range(1, 8)

        weekDays.forall(
          wD => {
            lShifts.exists((d,ls) => d.to == wD &&
              {
                // Obtains a flattened list of the nurse names of this day's shift list
                val nameList = ls.map(s => s.nurses.to.keySet).flatten.map(n => n.name)

                // Groups the names by occurrence
                val countOccurs = nameList.groupBy(identity).map((n,l) => (n,l.length))

                // Checks if there is any nurse that performs more shifts than allowed in that day
                !countOccurs.exists((_,c) => c > mspd.to)
              }
            )
          }
        )

      }
      )
    })

  /* Since the MS1 algorithm is not prepared to handle this constraint the property will fail for now */
  property("The nurses should at least not work the amount of days defined by minRestDaysPerWeek") =
    forAll(genValidScheduleInfo)( si => {
      scheduleAlg.generateSchedule(si).fold(e => true, ss => {
        val lShifts: Map[Day, Seq[Shift]] = ss.mapShifts.to
        val mrdpw = si.constraints.minRestDaysPerWeek

        // Create a Map that connects the day and the nurses that work that day
        val nurseDayList : Map[Day,Seq[NonEmptyName]] = lShifts.map((d,ls) => (d,ls.map(s => s.nurses.to.keySet).flatten.map(n => n.name)))

        // Obtain a flattened and distinct list of all nurses that work the schedule
        val nurseDistinctList = nurseDayList.values.flatten.toList.distinct

        nurseDistinctList.forall( // For each nurse
          n => {
            nurseDayList.count((d,l) => !l.contains(n)) >= mrdpw.to //  Count the days in which nurse does not work, check if their higher or equal to the minimum rest days
          }
        )

      })
    })

  /* Since the MS1 algorithm is not prepared to handle this constraint the property will fail for now */
  property("The created schedule should respect the nurse day preference") =
    forAll(genValidScheduleInfoWith1SimpleSP)( si => {
      scheduleAlg.generateSchedule(si).fold(e => true, ss => {
        val mrdpw = si.constraints.minRestDaysPerWeek

        // Create a map of (nurse,day) and it's respective weighted value from the day preference list
        val nurseDayWeightedList : Seq[(Nurse,Day,Int)] = si.listDayPreference.map((dp) => (dp.nurse,dp.day,dp.preference.to * dp.nurse.seniority.to))

        ss.mapShifts.to.forall(
          (elem) => {
            val d = elem._1
            val dayShiftNurses : Seq[Nurse] = elem._2.map((s) => s.nurses.to.keySet).flatten

            dayShiftNurses.forall(
              shiftN => {
                // Get the nurse weight if it is in the list, otherwise the value is 0
                val shiftNWeight = nurseDayWeightedList.find((ndw) => ndw._1 == shiftN && ndw._2 == d).getOrElse((shiftN,d,0))._3

                nurseDayWeightedList.forall((el : (Nurse, Day, Int)) => {
                  // Check if this nurse was selected for the day, if so there is no need to check her
                  dayShiftNurses.contains(el._1) ||
                    // If it is for a different day there is no need to check either
                    el._2 != d ||
                    // Make sure this nurse has a lower or equal weight than the one in the shift
                    (el._3 <= shiftNWeight )

                  // Check if there are no other nurses left with no preference and if the default weight doesn't surpass our nurses weight
                }) ||
                  ((0 > shiftNWeight) &&
                    !si.listNurses.exists(
                      (nurse) => {
                        !nurseDayWeightedList.contains(
                          (ndw1: (Nurse, Day, Int)) => {
                            ndw1._1 == nurse && ndw1._2 == d
                          })  // keeps only the nurses from listNurses that do not follow the condition
                        || !dayShiftNurses.contains(
                          (dsn: Nurse) => {
                            dsn == nurse
                          })
                      }))
              })
          }
        )
      })
    })
*/