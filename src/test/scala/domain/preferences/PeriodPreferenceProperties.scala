package domain.preferences

import domain.preferences.PeriodPreference
import domain.nurse.NurseProperties.*
import domain.CommonDomainTypesProperties.*
import domain.nurse.Nurse
import domain.schedulingPeriod.SchedulingPeriod
import org.scalacheck.*

object PeriodPreferenceProperties extends Properties("PeriodPreferenceProperties") :

  def genPeriodPreference(ln:List[Nurse])(lsp:List[SchedulingPeriod]): Gen[PeriodPreference] =
    for {
      randomNurse <- Gen.oneOf(ln)
      randomSp <- Gen.oneOf(lsp)
      randomPreference <- genPreference
    } yield PeriodPreference(randomNurse,randomSp,randomPreference)

  def genLPeriodPreference(ln:List[Nurse])(lsp:List[SchedulingPeriod]): Gen[List[PeriodPreference]] =
    Gen.choose(1,5).flatMap(s => Gen.listOfN(s, genPeriodPreference(ln)(lsp)))