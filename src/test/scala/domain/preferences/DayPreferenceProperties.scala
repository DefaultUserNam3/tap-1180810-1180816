package domain.preferences


import domain.preferences.DayPreference
import domain.nurse.NurseProperties.*
import domain.CommonDomainTypesProperties.*
import domain.nurse.Nurse
import org.scalacheck.*

object DayPreferenceProperties extends Properties("DayPreferenceProperties") :

  def genDayPreference(ln:List[Nurse]): Gen[DayPreference] =
    for {
      randomNurse <- Gen.oneOf(ln)
      randomDay <- genDay
      randomPreference <- genPreference
    } yield DayPreference(randomNurse,randomDay,randomPreference)

  def genLDayPreference(ln:List[Nurse]): Gen[List[DayPreference]] =
    Gen.choose(0,2).flatMap(s => Gen.listOfN(s, genDayPreference(ln)))

