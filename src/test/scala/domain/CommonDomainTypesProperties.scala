package domain

import domain.CommonDomainTypes.*
import org.scalacheck.*

object CommonDomainTypesProperties extends Properties("CommonDomainTypesProperties") :

  val MAX_QUANTITY = 10

  /**
   * Generates a non empty alphanumeric string
   * @return
   */
  def genNonEmptyString: Gen[String] =
    for {
      randomNum <- Gen.chooseNum(8, MAX_QUANTITY)
      nonEmptyCharList : Gen[List[Char]] <- Gen.listOfN(randomNum,Gen.alphaNumChar)
      str <- nonEmptyCharList.map(_.mkString)
    } yield str

  /**
   * Generates a NonEmptyId
   * @return
   */
  def genNonEmptyId: Gen[NonEmptyId] =
    for {
      randomStr <- genNonEmptyString
      randomNonEmptyId<- NonEmptyId.from(randomStr).fold(a => Gen.fail, b => Gen.const(b))
    } yield randomNonEmptyId

  /**
   * Generates a NonNegativeInt
   * @return
   */
  def genNonNegativeInt: Gen[NonNegativeInt] =
    for {
      randomNum <- Gen.chooseNum(0, MAX_QUANTITY)
      randomNonNegativeInt<- NonNegativeInt.from(randomNum).fold(a => Gen.fail, b => Gen.const(b))
    } yield randomNonNegativeInt

  /**
   * Generates a PositiveInt
   * @return
   */
  def genPositiveInt: Gen[PositiveInt] =
    for {
      randomNum <- Gen.chooseNum(1, MAX_QUANTITY)
      randomPositiveInt<- PositiveInt.from(randomNum).fold(a => Gen.fail, b => Gen.const(b))
    } yield randomPositiveInt

  /**
   * Generates a Preference
   * @return
   */
  def genPreference: Gen[Preference] =
    for {
      randomNum <- Gen.chooseNum(Preference.MIN_PREFERENCE, Preference.MAX_PREFERENCE)
      randomPreference<- Preference.from(randomNum).fold(a => Gen.fail, b => Gen.const(b))
    } yield randomPreference


  /**
   * Generates a Day
   * @return
   */
  def genDay: Gen[Day] =
    for {
      randomNum <- Gen.chooseNum(Day.MONDAY, Day.MONDAY + 6)
      randomDay<- Day.from(randomNum).fold(a => Gen.fail, b => Gen.const(b))
    } yield randomDay
