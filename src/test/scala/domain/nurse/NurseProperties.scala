package domain.nurse

import domain.nurse.Nurse
import domain.nurse.Role.*
import domain.CommonDomainTypesProperties.*
import domain.role.RoleProperties.genRole
import domain.schedulingPeriod.SchedulingPeriod
import domain.constraints.Constraints
import org.scalacheck.*
import org.scalacheck.Prop.forAll

import scala.collection.immutable.List.from
import scala.math.ceil

object NurseProperties extends Properties("NurseProperties") :

  val MAX_ROLES = 2

  def genNurse: Gen[Nurse] =
    for {
      randomNonEmptyName <- genNonEmptyName
      randomSeniority <- genSeniority
      randomNonEmptyRoleList <- genNonEmptyRoleList
    } yield Nurse(randomNonEmptyName,randomSeniority,randomNonEmptyRoleList)

  def genNurse(role:Role): Gen[Nurse] =
    for {
      randomNonEmptyName <- genNonEmptyName
      randomSeniority <- genSeniority
      roleList<- genNonEmptyRoleListWithSingleRole(role)
    } yield Nurse(randomNonEmptyName,randomSeniority,roleList)
  
  
  def genLNurses(req: (Role, Int)): Gen[List[Nurse]] =
    Gen.listOfN(req._2, genNurse(req._1))


  def genSeniority: Gen[Seniority] =
    for {
      randomNum <- Gen.chooseNum(Seniority.MIN_SENIORITY, Seniority.MAX_SENIORITY)
      randomSeniority <- Seniority.from(randomNum).fold(a => Gen.fail, b => Gen.const(b))
    } yield randomSeniority

  def genNonEmptyName: Gen[NonEmptyName] =
    for {
      randomStr <- genNonEmptyString
      randomNonEmptyName <- NonEmptyName.from(randomStr).fold(a => Gen.fail, b => Gen.const(b))
    } yield randomNonEmptyName

  def genNonEmptyRoleList: Gen[NonEmptyRoleList] =
    for {
      limsRoleList <- Gen.chooseNum(1,MAX_ROLES)
      randomRoleList <- Gen.listOfN(limsRoleList,genRole)
      randomNonEmptyRoleList <- NonEmptyRoleList.from(randomRoleList.distinct).fold(a => Gen.fail, b => Gen.const(b))
    } yield randomNonEmptyRoleList

  def genNonEmptyRoleListWithSingleRole(role:Role): Gen[NonEmptyRoleList] =
      NonEmptyRoleList.from(List(role)).fold(a => Gen.fail, b => Gen.const(b))


  /**
   * Recursivelly create generators of nurses that necessarily have a certain role and aggregate them
   * @param mr map of necessary quantity of nurses that MUST be able to perform the associated role
   * @return
   */
  def genLNursesX(mr: Map[Role,Int]): Gen[List[Nurse]] =
    if(mr.isEmpty) return List()
    for{
      ln <- genLNurses2(mr.head)
      lr <- genLNursesX(mr.tail)
    } yield from(ln) ::: from(lr)


  def genLNurses2(mr: (Role,Int)): Gen[List[Nurse]] =
    Gen.listOfN(mr._2, genNurse(mr._1))

  def genValidNursesForSchedulingPeriods( lsp : List[SchedulingPeriod], c : Constraints) : Gen[List[Nurse]] =
    val f = findQuantityNursesRequired(lsp)(c)
    // Try to avoid repeated nurses
    genLNursesX(f).retryUntil(ln =>
      ln.groupBy(_.name).map(_._2.head).size == ln.length, 30)

  def findQuantityNursesRequired(lsp:List[SchedulingPeriod])(c:Constraints): Map[Role,Int] =
    val aggrReqs = lsp.map(n => n.nurseRequirements.to.toList).flatten.groupMap(_._1)(_._2).map((k,v) => (k,v.max));
    // Approach 1: Pick the maximum and use it as the basis -> (NrOcurRole * MaxReq) / maxShiftsPerDay
    val roleOccur = lsp.map(n => n.nurseRequirements.to.toList).flatten.groupMap(_._1)(_._2).map((k,v) => (k,ceil(v.length * v.max / c.maxShiftsPerDay.to.toFloat).toInt));
    // Approach 2: Sum the role's requirements and use that -> Sum / maxShiftsPerDay
    //val roleOccur = lsp.map(n => n.nurseRequirements.to.toList).flatten.groupMap(_._1)(_._2).map((k,v) => (k,ceil(v.foldLeft(0)(_ + _) / c.maxShiftsPerDay.to.toFloat).toInt));
    //merge the maps, finding the max between the two
    val merged =(aggrReqs.toSeq ++ roleOccur.toSeq).groupMapReduce(_._1)(_._2)(math.max(_,_))
    //Add the necessary extra nurses depending on the rest days per week
    // (Req*7)/(7-Min)
    merged.map((k,v) => k -> ceil((v*7)/(7-c.minRestDaysPerWeek.to.toFloat)).toInt)

