package domain.role

import domain.CommonDomainTypesProperties.*
import domain.nurse.Role
import org.scalacheck.*

object RoleProperties extends Properties("RoleProperties") :

  def genRole: Gen[Role] =
    for {
      randomNonEmptyTitle <-  Gen.oneOf("RoleA", "RoleB", "RoleC", "RoleD")
      randomRole<- Role.from(randomNonEmptyTitle).fold(a => Gen.fail, b => Gen.const(b))
    } yield randomRole

