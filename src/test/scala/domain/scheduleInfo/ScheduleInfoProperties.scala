package domain.scheduleInfo

import org.scalacheck.*
import org.scalacheck.Properties
import domain.schedulingPeriod.SchedulingPeriodProperties.*
import domain.nurse.NurseProperties.*
import domain.preferences.PeriodPreferenceProperties.*
import domain.preferences.DayPreferenceProperties.*
import domain.constraints.ConstraintsProperties.*

object ScheduleInfoProperties extends Properties("ScheduleInfoProperties") :

  def genValidScheduleInfo: Gen[ScheduleInfo] =
    for {
      randomLSP <- genLSchedulingPeriods
      randomC <- genConstraints
      randomLN <- genValidNursesForSchedulingPeriods(randomLSP,randomC)
      randomDP <- genLDayPreference(randomLN)
      randomPP <- genLPeriodPreference(randomLN)(randomLSP)
    } yield ScheduleInfo(randomLSP,randomLN,randomPP,randomDP,randomC)

  /**
   * Generates a valid Schedule Info with only 1 scheduling period that has 1 role
   * @return valid Schedule Info
   */
  def genValidScheduleInfoWith1SimpleSP: Gen[ScheduleInfo] =
    for {
      randomLSP <- Gen.listOfN(1,genSchedulingPeriod1Role)
      randomC <- genConstraints
      randomLN <- genValidNursesForSchedulingPeriods(randomLSP,randomC)
      randomDP <- genLDayPreference(randomLN)
      randomPP <- genLPeriodPreference(randomLN)(randomLSP)
    } yield ScheduleInfo(randomLSP,randomLN,randomPP,randomDP,randomC)
