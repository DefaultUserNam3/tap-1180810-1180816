package domain.constraints

import domain.constraints.Constraints
import domain.CommonDomainTypesProperties.*
import org.scalacheck.*

object ConstraintsProperties extends Properties("ConstraintsProperties") :

  val MIN_CONSTRAINT = 0
  val MAX_CONSTRAINT = 1
  
  def genConstraints: Gen[Constraints] =
    for {
      randomMRDPR <- Gen.chooseNum(MIN_CONSTRAINT,MAX_CONSTRAINT)
      randomMSPD <- Gen.chooseNum(1,MAX_CONSTRAINT)
      randomConstraints <- Constraints.from(randomMRDPR,randomMSPD).fold(a => Gen.fail, b => Gen.const(b))
    } yield randomConstraints
