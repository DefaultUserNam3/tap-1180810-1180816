import org.scalacheck.Gen
import domain.CommonDomainTypesProperties.*
import domain.nurse.Nurse
import domain.nurse.Role
import domain.schedule.ScheduleMS03



import domain.scheduleInfo.ScheduleInfoProperties.*

val g = genValidScheduleInfo
val r = g.sample
println(ScheduleMS03.generateSchedule(r.get))

//val l = List(List(("Nurse(ZLcdfxRviO,1,List(RoleA))","RoleA"), ("Nurse(rLk229dp,1,List(RoleA))","RoleA"), ("Nurse(pySxarhc,1,List(RoleA))","RoleA")), List(("Nurse(ZLcdfxRviO,1,List(RoleA)","RoleA"), ("Nurse(rLk229dp,1,List(RoleA))","RoleA"), ("Nurse(eUrlEpnaF,3,List(RoleA))","RoleA")), List(("Nurse(ZLcdfxRviO,1,List(RoleA))","RoleA"), ("Nurse(rLk229dp,1,List(RoleA))","RoleA"), ("Nurse(kM3Pma7k,5,List(RoleA))","RoleA")))
//val l2 = List(List(("Nurse(ZLcdfxRviO,1,List(RoleA))","RoleB"), ("Nurse(rLk229dp,1,List(RoleA))","RoleB"), ("Nurse(pySxarhc,1,List(RoleA))","RoleB")), List(("Nurse(ZLcdfxRviO,1,List(RoleA)","RoleB"), ("Nurse(rLk229dp,1,List(RoleA))","RoleB"), ("Nurse(eUrlEpnaF,3,List(RoleA))","RoleB")), List(("Nurse(ZLcdfxRviO,1,List(RoleA))","RoleB"), ("Nurse(rLk229dp,1,List(RoleA))","RoleB"), ("Nurse(kM3Pma7k,5,List(RoleA))","RoleB")))
//val l3 = List(List(("Nurse(ZLcdfxRviO,1,List(RoleA))","RoleC"), ("Nurse(rLk229dp,1,List(RoleA))","RoleC"), ("Nurse(pySxarhc,1,List(RoleA))","RoleC")), List(("Nurse(ZLcdfxRviO,1,List(RoleA)","RoleC"), ("Nurse(rLk229dp,1,List(RoleA))","RoleC"), ("Nurse(eUrlEpnaF,3,List(RoleA))","RoleC")), List(("Nurse(ZLcdfxRviO,1,List(RoleA))","RoleC"), ("Nurse(rLk229dp,1,List(RoleA))","RoleC"), ("Nurse(kM3Pma7k,5,List(RoleA))","RoleC")))
//val ll = List(l) ::: List(l2)
//val lll = ScheduleMS03.combinationList(ll)
//println(lll.head.flatten.toMap)
//println(ScheduleMS03.combinationList(ll).flatten)
//
//println("teste")
